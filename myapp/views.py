from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def home_page(request):
    return render(request,'home_page.html')

def about(request):
    return render(request,'about.html')
    

def aboutweb(request):
    return render(request,'aboutweb.html')